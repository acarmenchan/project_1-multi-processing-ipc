import java.util.ArrayList;
import java.util.Random;

public class CPU 
{
	public enum CPU_MODE
	{
		USER,
		KERNEL
	}
	
	private boolean DEBUG_MODE = false;
	
	private int timerInterruptValue;
	private CPU_MODE cpuMode;
	private Memory memory = new Memory();

	// REGISTERS
	private int PC, // Program Counter (points to memory address in memoryArray 0-1999)
				SP, // Stack Pointer
				IR, // Instruction Register
				AC, // Accumulator
				X,  // General purpose register
				Y;  // General purpose register
	
	public CPU(int timerInterruptValue)
	{
		this.timerInterruptValue = 0;
		this.cpuMode = CPU_MODE.USER;
		
		this.PC = 0;
		this.SP = 999; // points to end of user memory
		this.IR = 0;
		this.AC = 0;
		this.X  = 0;
		this.Y  = 0;
	}

	public void loadProgram(ArrayList<String> cleanProgramFile) 
	{
		int address = 0; 
		
		for (String fileLine : cleanProgramFile)
		{
			if (fileLine.charAt(0) == '.')
			{
				//remove the '.'
				address = Integer.valueOf(fileLine.substring(1, fileLine.length()));
				continue;
			}
			
			//no '.' to remove
			memory.write(address, Integer.valueOf(fileLine));
			address++;
		}
	}

	public void runProgram() 
	{
		System.out.println();
		System.out.println();
		System.out.println("//*****************************//");
		System.out.println("// BEGINNING PROGRAM EXECUTION //");
		System.out.println("//*****************************//");
		System.out.println();
		
		int instructionCounter = 0;
		
		while (true)
		{
			this.IR = memory.read(this.PC);
			if ( DEBUG_MODE ) { System.out.println("Instruction " + this.IR); }

			if (this.IR != 50)
			{
				performInstruction();
				instructionCounter++;
				this.PC++;
				
				//immediately check if we need to perform a timer interrupt
				if (instructionCounter == this.timerInterruptValue)
				{
					this.PC--; //bring the PC back down so it is pointing to the correct line of execution to return to
					this.timerInterrupt();
					instructionCounter = 0;
				}	
			}
			else
			{
				if ( DEBUG_MODE ) { System.out.println("Ending execution..."); }
				break;
			}
		}
	}

	private void performInstruction() 
	{
		switch (this.IR) 
		{
			case 1:  this.loadValue();   	 break;
			case 2:  this.loadAddress(); 	 break;
			case 3:  this.loadIndex();   	 break;
			case 4:  this.loadIndex_X(); 	 break;
			case 5:  this.loadIndex_Y(); 	 break;
			case 6:  this.loadSP_X();    	 break;
			case 7:  this.store();		     break;
			case 8:  this.getRandomNumber(); break;
			case 9:  this.putPort();		 break;
			case 10: this.addX();			 break;
			case 11: this.addY();			 break;
			case 12: this.subtractX(); 	 	 break;
			case 13: this.subtractY(); 	 	 break;
			case 14: this.copyToX();		 break;
			case 15: this.copyFromX();		 break;
			case 16: this.copyToY();		 break;
			case 17: this.copyFromY(); 		 break;
			case 18: this.copyToSP();		 break;
			case 19: this.copyFromSP(); 	 break;
			case 20: this.jump();			 break;
			case 21: this.jumpIfEqual(); 	 break;
			case 22: this.jumpIfNotEqual();  break;
			case 23: this.functionCall(); 	 break;
			case 24: this.functionReturn();  break;
			case 25: this.incrementX();		 break;
			case 26: this.decrementX();		 break;
			case 27: this.push(); 			 break;
			case 28: this.pop(); 			 break;
			case 29: this.interrupt(); 		 break;
			case 30: this.interruptReturn(); break;
	
			default:						 
				break;
		}
	}

	private void raiseMemoryAccessException(int address)
	{
		if (address < 0 || address >= 2000)
		{
			System.out.println("Memory violation: accessing address " + address + " outside of available memory space");
		}
		else if (this.cpuMode == CPU_MODE.USER)
		{
			System.out.println("Memory violation: accessing system address " + address + " in user mode");
		}
		else if (this.cpuMode == CPU_MODE.KERNEL)
		{
			System.out.println("Memory violation: accessing system address " + address + " in kernel mode");
		}
	}
	
	private void checkMemoryAccess(int address)
	{
		if( this.cpuMode == CPU_MODE.USER && address >= 1000 )
		{
			this.raiseMemoryAccessException(address);
		}
		
		if( this.cpuMode == CPU_MODE.KERNEL && address < 1000 )
		{
			this.raiseMemoryAccessException(address);
		}
	}
	
	/**
	 * Instruction 1 This function stores a value into the AC register
	 * 
	 * @param value - an integer
	 */
	private void loadValue() 
	{	// cpu.ac = value;
		
		this.PC++;
		this.checkMemoryAccess(this.PC);
		this.AC = memory.read(this.PC);
		if ( DEBUG_MODE ) { System.out.println("Loading value " + memory.read(this.PC) + " into AC"); }
	}

	/**
	 * Instruction 2 This function stores the value at the address in memoryArray into the AC register
	 * 
	 * @param address - memoryArray element/index 0-1999
	 */
	private void loadAddress() 
	{	// take value from memoryArray and call loadValue func
		// int value = memoryArray[address]
		// loadValue(value)
		
		this.PC++;
		this.checkMemoryAccess(this.PC);
		this.checkMemoryAccess(memory.read(this.PC));
		this.AC = memory.read(memory.read(this.PC));
		if ( DEBUG_MODE ) { System.out.println("Loading value " + this.AC + " from address " + memory.read(this.PC) + " into AC"); }
	}

	/**
	 * Instruction 3 This function stores the value from the address found in the given address in memoryArray into the AC register
	 * 
	 * @param address - memoryArray element/index 0-1999
	 */
	private void loadIndex() 
	{	// newAddress = memoryArray[address]
		// loadAddress(newAddress)
		// loadValue memoryArray[memory[address]]
		
		this.PC++;
		this.checkMemoryAccess(this.PC);
		this.checkMemoryAccess(memory.read(this.PC));
		this.checkMemoryAccess(memory.read(memory.read(this.PC)));
		this.AC = memory.read(memory.read(memory.read(this.PC)));
		if ( DEBUG_MODE ) { System.out.println("Loading value " + this.AC + " from address " + memory.read(memory.read(this.PC)) + " found in address " + memory.read(this.PC) + " into AC"); }
	}

	/**
	 * Instruction 4 This function adds the index to the value stored in the X register and stores it in the AC register
	 * 
	 * @param index - the value to add to the number stored in the X register
	 */
	private void loadIndex_X() 
	{	// AC = index + X_register
		// address = index + X_register
		
		this.PC++;
		this.checkMemoryAccess(this.PC);
		this.checkMemoryAccess(memory.read(this.PC) + this.X);
		this.AC = memory.read(memory.read(this.PC) + this.X);
		if ( DEBUG_MODE ) { System.out.println("Loading value " + this.AC + " at address " + (memory.read(this.PC) + this.X) + " into AC"); }
	}

	/**
	 * Instruction 5 This function adds the index to the value stored in the Y register and stores it in the AC register
	 * 
	 * @param index - the value to add to the number stored in the Y register
	 */
	private void loadIndex_Y() 
	{	// AC = index + Y_register
		// address = index + Y_register
		
		this.PC++;
		this.checkMemoryAccess(this.PC);
		this.checkMemoryAccess(memory.read(this.PC) + this.Y);
		this.AC = memory.read(memory.read(this.PC) + this.Y);
		if ( DEBUG_MODE ) { System.out.println("Loading value " + this.AC + " at address " + (memory.read(this.PC) + this.Y) + " into AC"); }
	}

	/**
	 * Instruction 6 This function adds the value in the SP register to the value in the X register and stores it in the AC register
	 */
	private void loadSP_X() 
	{	// AC = SP_register + X_register
		// address = SP_register + X_register
		
		this.checkMemoryAccess(this.SP + this.X);
		this.AC = memory.read(this.SP + this.X);
		if ( DEBUG_MODE ) { System.out.println("Loading value " + this.AC + " at address " + (this.SP + this.X) + " into AC"); }
	}

	/**
	 * Instruction 7 This function takes the value in the AC register and stores it into a passed memoryArray element.
	 * 
	 * @param address - memoryArray index
	 */
	private void store() 
	{	// memoryArray[address] = AC
		
		this.PC++;
		this.checkMemoryAccess(this.PC);
		memory.write(memory.read(this.PC), this.AC);
		if ( DEBUG_MODE ) { System.out.println("Storing value in AC into address " + memory.read(this.PC)); }
	}

	/**
	 * Instruction 8 This function generates a random integer from 1 to 100 and stores it in the AC register
	 */
	private void getRandomNumber() 
	{
		Random random = new Random();
		int randomNumber = random.nextInt(100) + 1;
		this.AC = randomNumber;
		if ( DEBUG_MODE ) { System.out.println("Generating random number... " + this.AC + " and storing into AC"); }
	}

	/**
	 * Instruction 9 This function reads the parameter and will output either an integer or character to console
	 * 
	 * @param - passes 1 for integer or 2 for character
	 */
	private void putPort() 
	{
		this.PC++;
		if ( DEBUG_MODE ) { System.out.println("Putting port " + memory.read(this.PC)); }
		
		this.checkMemoryAccess(this.PC);
		if (memory.read(this.PC) == 1)
		{
			System.out.print(this.AC);		
		}
		
		if (memory.read(this.PC) == 2)
		{
			// convert int to ascii character
			char asciiChar = (char) this.AC;
			System.out.print(asciiChar);
		}
	}

	/**
	 * Instruction 10 This function adds the value in the X register to the AC register
	 */
	private void addX() 
	{	// AC += X_register
		
		if ( DEBUG_MODE ) { System.out.println("Adding " + this.X + " (X) to " + this.AC + " (AC)"); }
		this.AC += this.X;
	}

	/**
	 * Instruction 11 This function adds the value in the Y register to the AC register
	 */
	private void addY() 
	{	// AC += Y_register
		
		if ( DEBUG_MODE ) { System.out.println("Adding " + this.Y + " (Y) to " + this.AC + " (AC)"); }
		this.AC += this.Y;
	}

	/**
	 * Instruction 12 This function subtracts the value in the X register from the AC register
	 */
	private void subtractX() 
	{	// AC -= X_register
		
		if ( DEBUG_MODE ) { System.out.println("Subtracting " + this.X + " (X) from " + this.AC + " (AC)"); }
		this.AC -= this.X;
	}

	/**
	 * Instruction 13 This function subtracts the value in the Y register from the AC register
	 */
	private void subtractY() 
	{	// AC -= Y_register
		
		if ( DEBUG_MODE ) { System.out.println("Subtracting " + this.Y + " (Y) from " + this.AC + " (AC)"); }
		this.AC -= this.Y;
	}

	/**
	 * Instruction 14 This function copies the value in the AC register to the X register variable
	 */
	private void copyToX() 
	{	// X_register = AC_register
		
		if ( DEBUG_MODE ) { System.out.println("Copying " + this.AC + " (AC) to " + this.X + " (X)"); }
		this.X = this.AC;
	}

	/**
	 * Instruction 15 This function copies the value in the X register to the AC register
	 */
	private void copyFromX() 
	{	// AC_register = X_register
		
		if ( DEBUG_MODE ) { System.out.println("Copying " + this.X + " (X) to " + this.AC + " (AC)"); }
		this.AC = this.X;
	}

	/**
	 * Instruction 16 This function copies the value in the AC register to the Y register variable
	 */
	private void copyToY() 
	{	// Y_register = AC_register
		
		if ( DEBUG_MODE ) { System.out.println("Copying " + this.AC + " (AC) to " + this.Y + " (Y)"); }
		this.Y = this.AC;
	}

	/**
	 * Instruction 17 This function copies the value in the Y register to the AC register
	 */
	private void copyFromY() 
	{	// AC_register = Y_register
		
		if ( DEBUG_MODE ) { System.out.println("Copying " + this.Y + " (Y) to " + this.AC + " (AC)"); }
		this.AC = this.Y;
	}

	/**
	 * Instruction 18 This function copies the value in the AC register to the SP register variable
	 */
	private void copyToSP() 
	{	// SP_register = AC_register
		
		if ( DEBUG_MODE ) { System.out.println("Copying " + this.AC + " (AC) to " + this.SP + " (SP)"); }
		this.SP = this.AC;
	}

	/**
	 * Instruction 19 This function copies the value in the SP register to the AC register
	 */
	private void copyFromSP() 
	{	// AC_register = SP_register
		
		if ( DEBUG_MODE ) { System.out.println("Copying " + this.SP + " (SP) to " + this.AC + " (AC)"); }
		this.AC = this.SP;
	}

	/**
	 * Instruction 20 This function assigns the PC register to an address from memoryArray
	 * 
	 * @param address - element/index in memoryArray
	 */
	private void jump() 
	{	// PC_register = address
		
		this.PC++;
		if ( DEBUG_MODE ) { System.out.println("Jumping to address " + memory.read(this.PC)); }
		this.checkMemoryAccess(this.PC);
		this.PC = memory.read(this.PC);
		this.PC--; // keep pointer at instruction at jump address
	}

	/**
	 * Instruction 21 This function assigns the PC register to an address from memoryArray if the value in the AC register is zero
	 * 
	 * @param address - element in memoryArray
	 */
	private void jumpIfEqual() 
	{	// if (AC_register == 0)
		// PC_register = address
		
		this.PC++;
		if ( DEBUG_MODE ) { System.out.println("Jumping to address " + memory.read(this.PC) + " if " + this.AC + "(AC) == 0"); }
		
		if (this.AC == 0)
		{
			this.checkMemoryAccess(this.PC);
			this.PC = memory.read(this.PC);
			this.PC--; // keep pointer at instruction at jump address
		}
	}

	/**
	 * Instruction 22 This function assigns the PC register to an address from memoryArray if the value in the AC register is not zero
	 * 
	 * @param address - element in memoryArray
	 */
	private void jumpIfNotEqual() 
	{	// if (AC_register != 0)
		// PC_register = address
		
		this.PC++;
		if ( DEBUG_MODE ) { System.out.println("Jumping to address " + memory.read(this.PC) + " if " + this.AC + "(AC) != 0"); }
		
		if (this.AC != 0)
		{
			this.checkMemoryAccess(this.PC);
			this.PC = memory.read(this.PC);
			this.PC--; // keep pointer at instruction at jump address
		}
	}

	/**
	 * Instruction 23 This function pushes the return address onto stack and jumps to the address
	 * 
	 * @param address
	 */
	private void functionCall() 
	{
		this.PC++;
		if ( DEBUG_MODE ) { System.out.println("Pushing return address to stack and jumping to address " + memory.read(this.PC)); }
		this.SP--;
		memory.write(this.SP, this.PC); //save the PC on the stack
		this.checkMemoryAccess(this.PC);
		this.PC = memory.read(this.PC); //jump to the specified address
		this.PC--; // keep pointer at instruction at jump address
	}

	/**
	 * Instruction 24 This function pops the return address from the stack and jumps to the address
	 */
	private void functionReturn() 
	{
		if (DEBUG_MODE) { System.out.println("Popping return address from stack and jumping to address"); }
		this.checkMemoryAccess(this.SP);
		this.PC = this.memory.read(this.SP);
		this.SP++;
		this.PC--; // keep pointer at instruction at jump address
		
	}

	/**
	 * Instruction 25 This function increments the value in the X register
	 */
	private void incrementX() 
	{	// X_register++
		
		this.X++;
		if (DEBUG_MODE) { System.out.println("Incrementing X, X is now " + this.X); }
	}

	/**
	 * Instruction 26 This function decrements the value in the X register
	 */
	private void decrementX() 
	{	// X_register--
		
		this.X--;
		if (DEBUG_MODE) { System.out.println("Decrementing X, X is now " + this.X); }
	}

	/**
	 * Instruction 27 This function pushes the value in the AC register onto the stack
	 */
	private void push() 
	{
		this.memory.write(this.SP, this.AC);
		this.SP--;
		if (DEBUG_MODE) { System.out.println("Pushing AC onto stack"); }
	}

	/**
	 * Instruction 28 This function pops the value from the stack into the AC register
	 */
	private void pop() 
	{
		this.SP++;
		this.AC = this.memory.read(this.SP);
		if (DEBUG_MODE) { System.out.println("Popping from stack into AC"); }
	}

	/**
	 * Instruction 29 This function performs a system call (interrupt) and causes execution at address 1500
	 */
	private void interrupt() 
	{
		if (DEBUG_MODE) { System.out.println("Performing system call"); }
		
		int savedSP = this.SP;
		if( this.cpuMode == CPU_MODE.USER )
		{
			this.SP = 1999;
		}
		this.cpuMode = CPU_MODE.KERNEL;
		
		// SAVE PC & IR VALUES FROM USER PROGRAM BEFORE MOVING TO SYSTEM SPACE!!!!
		this.memory.write(this.SP, savedSP); //save the SP at bottom of system memory space
		this.SP--;							 //move up the stack
		this.memory.write(this.SP, this.PC); //save the PC 
		this.SP--;							 //move up the stack
		
		this.PC = 1500; //starting execution at address 1500 for regular interrupt
		this.PC--; // keep pointer at instruction at jump address
	}

	/**
	 * This function will cause execution at address 1000 and will be called 
	 * manually in the runProgram() after every timerInterruptValue (30) instructions
	 */	
	private void timerInterrupt()
	{
		if (DEBUG_MODE) { System.out.println("Performing timer interrupt"); }
		
		int savedSP = this.SP;
		if( this.cpuMode == CPU_MODE.USER )
		{
			this.SP = 1999;
		}
		this.cpuMode = CPU_MODE.KERNEL;
		
		// SAVE PC & IR VALUES FROM USER PROGRAM BEFORE MOVING TO SYSTEM SPACE!!!!
		this.memory.write(this.SP, savedSP); //save the SP at bottom of system memory space
		this.SP--;							 //move up the stack
		this.memory.write(this.SP, this.PC); //save the PC 
		this.SP--;							 //move up the stack
		
		this.PC = 1000; //starting execution at address 1000 for timer interrupt
	}
	
	/**
	 * Instruction 30 This function returns from the system call (interrupt)
	 */
	private void interruptReturn() 
	{	
		if (DEBUG_MODE) { System.out.println("Returning from system call"); }
		
		this.cpuMode = CPU_MODE.USER;
		
		// LOAD PC VALUES FROM SYSTEM STACK BEFORE MOVING BACK TO USER SPACE!!!!
		this.SP++;							//move down the stack
		this.PC = this.memory.read(this.SP);//return original PC value	
		this.SP++;							//move down the stack - SP is now at bottom of the system stack
		this.SP = this.memory.read(this.SP);//return original SP value
		
		if (this.PC >= 1000 && this.PC < 2000)
		{
			this.cpuMode = CPU_MODE.KERNEL;
		}
	}

	// ***** DEBUG FUNCTIONS *****//
	public void DEBUG_dumpMemory() 
	{
		System.out.println();

		for (int i = 0; i < this.memory.MEMORY_SIZE; i++) {
			System.out.println(this.memory.read(i));
		}
	}
}
