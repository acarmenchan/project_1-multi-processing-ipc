import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Main 
{	
	private static boolean DEBUG_MODE = true;
	
	public static void main(String[] args) 
	{		
		String fileName = args[0];
		int timerInterruptValue = Integer.valueOf(args[1]);
		
		System.out.println("The timer interrup value is " + timerInterruptValue);
		
		ArrayList<String> rawProgramFile = loadProgram(fileName); // load program file from disk
		ArrayList<String> cleanProgramFile = sanitizeProgram(rawProgramFile); // remove comments from text file
		
		if( DEBUG_MODE ) { DEBUG_printProgram(cleanProgramFile); }
			
		CPU cpu = new CPU(timerInterruptValue);
		cpu.loadProgram(cleanProgramFile);
		
//		if( DEBUG_MODE ) { cpu.DEBUG_dumpMemory(); }
		
		cpu.runProgram();
}
	
	static ArrayList<String> loadProgram(String fileName)
	{
		System.out.println();
		System.out.println();
		System.out.println("================================");
		System.out.println("LOADING FILE " + fileName);
		System.out.println("================================");
		
		String lineIn;
		ArrayList<String> rawProgramFile = new ArrayList<String>();
		
		// OPEN INPUT FILE
		File inFile = new File (fileName);
		Scanner fileScan;
		
		try 
		{
			fileScan = new Scanner(inFile);
			
			// ADD LINES TO ARRAY LIST
			while (fileScan.hasNextLine()) 
			{
				lineIn = fileScan.nextLine();
				rawProgramFile.add(lineIn);
			}
			
			// CLOSE INPUT FILE
			fileScan.close();
			
		} catch (FileNotFoundException e) 
		{
			System.out.println("Could not find file named: " + fileName);
		}
		
		return rawProgramFile;
	}
	
	static ArrayList<String> sanitizeProgram(ArrayList<String> rawProgramFile)
	{
		ArrayList<String> cleanProgramFile = new ArrayList<String>();

		for (String fileLine : rawProgramFile)
		{
			String newLine = "";
			
			for (int i = 0; i < fileLine.length(); i++)
			{
				if (i == 0 && // dots are only allowed in first space
					(fileLine.charAt(i) == '.' || isDigit(fileLine.charAt(i))))
				{
					newLine += fileLine.charAt(i);
				}
				// i = 1 and on
				else if (isDigit(fileLine.charAt(i))) 
				{
					newLine += fileLine.charAt(i);
				}
				
				else
					break;
			}
			cleanProgramFile.add(newLine);
		}
		
		System.out.println("Deleting blank lines...");
		System.out.println();
		cleanProgramFile.removeAll(Arrays.asList(null,""));
		
		System.out.println("Finished sanitizing program!");

		return cleanProgramFile;
	}
	
	static boolean isDigit(char character)
	{
		return (character >= '0' && character <= '9');
	}
	
	static void DEBUG_printProgram(ArrayList<String> program)
	{
		System.out.println();
		System.out.println();
		System.out.println("================================");
		System.out.println("<DEBUG> PRINTING PROGRAM <DEBUG>");
		System.out.println("================================");
		
		int lineNumber = 1;
		for (String line : program) 
		{
			System.out.println(lineNumber + " : " + line);
			lineNumber++;
		}
	}
}
