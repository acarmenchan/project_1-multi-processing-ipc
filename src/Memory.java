import java.util.ArrayList;

public class Memory 
{
	public int MEMORY_SIZE = 2000;
	
	private ArrayList<Integer> memoryArray = new ArrayList<Integer>(MEMORY_SIZE); // 0-999 for the user program, 1000-1999 for system code.
			
	public Memory()
	{
		this.memoryArray = new ArrayList<Integer>(this.MEMORY_SIZE);
		for (int i = 0; i < 2000; i++)
		{
			this.memoryArray.add(0);
		}
	}

	public Integer read(int address)
	{
		return this.memoryArray.get(address);
	}
	
	public void write(int address, int data)
	{
		this.memoryArray.set(address, data);
	}
}
